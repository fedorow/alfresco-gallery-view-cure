# Alfresco Share Gallery View Cure

This is the cure for Alfresco Share Gallery View [issue](https://alfresco.atlassian.net/browse/SHA-2063). As I can find out, it's a problem of 5.2-7.1 versions. This wokraround tested only on 6.2 - 7.1 version. You should compare css file with your version before use it.

## Issue
As you can see on the screanshot bellow, the thumbnails are smaller then it should be and cut. The `alf-gallery-slider` change only width of thumbnails, the height stay as it is small and thumbnails do not change.

![It was](./img/2021-12-04_00-23.png)

## Cure
The [documentlist_v2.css](./documentlist_v2.css) file is Alfresco file with some changers to resolve the issue. It must replace original tomcat/webapps/share/components/documentlibrary/documentlist_v2.css file. It resolve the issue with size of thumbnails and alligen it to center, as it shown on the screanshot bellow.

It is bad practice to replace file directly, like it did in [Dockerfile](./Dockerfile) example. It is better to use this file in a [custom theme](https://docs.alfresco.com/content-services/7.0/tutorial/share/style/) or in the [Surf Extension module](https://docs.alfresco.com/content-services/latest/develop/share-ext-points/surf-extension-modules/).

![Became](./img/2021-12-04_00-39.png)
