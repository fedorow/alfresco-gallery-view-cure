FROM alfresco/alfresco-share:7.1.0.1

# Fix the gallery view thumbnails size
COPY documentlist_v2.css /usr/local/tomcat/webapps/share/components/documentlibrary/
